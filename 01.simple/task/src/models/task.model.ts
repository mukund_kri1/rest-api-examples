import {Entity, model, property} from '@loopback/repository';

@model()
export class Task extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  text: string;

  @property({
    type: 'boolean',
    required: true,
    default: false,
  })
  done: boolean;

  constructor(data?: Partial<Task>) {
    super(data);
  }
}
